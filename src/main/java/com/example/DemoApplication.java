package com.example;

import com.example.configuration.BeanConfiguration;
import com.example.database.parent.SubcategoryParent;
import com.example.database.productEntity.Notebook;
import com.example.database.productEntity.Smart;
import com.example.database.request.HibernateUtil;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.web.multipart.MultipartResolver;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;

@SpringBootApplication
public class DemoApplication  {

	public static void main(String[] args)
	{
		ApplicationContext ctx = SpringApplication.run(DemoApplication.class, args);
	}
}
